package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
