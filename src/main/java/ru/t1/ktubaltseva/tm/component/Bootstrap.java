package ru.t1.ktubaltseva.tm.component;

import ru.t1.ktubaltseva.tm.api.controller.ICommandController;
import ru.t1.ktubaltseva.tm.api.controller.IProjectController;
import ru.t1.ktubaltseva.tm.api.controller.IProjectTaskController;
import ru.t1.ktubaltseva.tm.api.controller.ITaskController;
import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;
import ru.t1.ktubaltseva.tm.controller.CommandController;
import ru.t1.ktubaltseva.tm.controller.ProjectController;
import ru.t1.ktubaltseva.tm.controller.ProjectTaskController;
import ru.t1.ktubaltseva.tm.controller.TaskController;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void initDemoData() throws AbstractException {
        projectService.add(new Project("pn2", "pd2", Status.IN_PROGRESS));
        projectService.add(new Project("pn5", "pd5", Status.NOT_STARTED));
        projectService.add(new Project("pn3", "pd3", Status.IN_PROGRESS));
        projectService.add(new Project("pn4", "pd4", Status.COMPLETED));

        taskService.add(new Task("tn2", "td2", Status.IN_PROGRESS));
        taskService.add(new Task("tn5", "td5", Status.NOT_STARTED));
        taskService.add(new Task("tn3", "td3", Status.IN_PROGRESS));
        taskService.add(new Task("tn4", "td4", Status.COMPLETED));
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private boolean processArgument(final String[] args) throws ArgumentNotSupportedException {
        if (args == null || args.length < 1) return false;
        final String argument = args[0];
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displaySystemInfo();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
        return true;
    }

    private void processCommand(final String command) throws AbstractException {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                commandController.displayVersion();
                break;
            case CommandConst.ABOUT:
                commandController.displayAbout();
                break;
            case CommandConst.HELP:
                commandController.displayHelp();
                break;
            case CommandConst.INFO:
                commandController.displaySystemInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_DISPLAY_BY_ID:
                projectController.displayProjectById();
                break;
            case CommandConst.PROJECT_DISPLAY_BY_INDEX:
                projectController.displayProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.displayProjects();
                break;
            case CommandConst.PROJECT_LIST_FULL_INFO:
                projectController.displayProjectsFullInfo();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_DISPLAY_BY_ID:
                taskController.displayTaskById();
                break;
            case CommandConst.TASK_DISPLAY_BY_INDEX:
                taskController.displayTaskByIndex();
                break;
            case CommandConst.TASK_DISPLAY_BY_PROJECT_ID:
                taskController.displayTasksByProjectId();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_LIST:
                taskController.displayTasks();
                break;
            case CommandConst.TASK_LIST_FULL_INFO:
                taskController.displayTasksFullInfo();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void run(final String[] args) throws AbstractException {
        if (processArgument(args)) return;
        initDemoData();
        initLogger();
        processCommands();
    }

}
